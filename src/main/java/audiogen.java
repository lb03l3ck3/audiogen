



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import math.jwave.Transform;
import math.jwave.transforms.DiscreteFourierTransform;

import org.apache.commons.lang3.ArrayUtils;




public class audiogen 
{

	public static void main(String[] args) throws URISyntaxException, UnsupportedAudioFileException, IOException
	{
		float[] fstep = {20f, 30f , 40f, 50f};
		audiogen audio = new audiogen();
		audio.generateAudioWin("C:\\Users\\admin\\Desktop\\neu",fstep);
	}
	
	
	public audiogen() 
	{
		// TODO Auto-generated constructor stub
	}
	
	
	public void generateAudio(String filename, float[]fStep) throws URISyntaxException, UnsupportedAudioFileException, IOException
	{
   		int sampleRate = 44100;		// in Hz
		int duration = 120;			// in s
		String audioName = filename.concat(".wav");
		
		int samples = duration * sampleRate;
		 
		
		for(int i = 0;  i<= fStep.length-1; i++)
		{
			fStep[i] = 2f/60f*fStep[i];
		}
		
	 
		
		double[] dirac = new double[]{};
		for(float f : fStep)
		{
			double[] data = new double[samples];
			
			int[] indices = new int[(int) (samples/(sampleRate/(f)))];
		
			for( int i = 0; i < indices.length; i++)
			{
				indices[i] = (int) (i * (sampleRate/(f)));
			}
		
			for(int i : indices)
			{
				data[i] = 1f;
			}
			
			
			
			dirac = ArrayUtils.addAll(dirac, data);
			
		}

		
		// normal velocity
		
		double[] beepArray = loadBeep();
		
		
		
		Transform trans = new Transform(new DiscreteFourierTransform());
		
		double[] beepFFT = trans.forward(beepArray);
		double[] diracFFT = trans.forward(dirac);
	
		int da  = diracFFT.length;
	
		
	}
	
	public void generateAudioWin(String filename, float[] fStep)
	{
		
		URL exeUrl = this.getClass().getClassLoader().getResource("exe.exe");
		File generator = new File(exeUrl.getPath());
		String gen  = generator.getAbsoluteFile().toString();
		
		URL	beepUrl = this.getClass().getResource("beep.wav");
		File soundFile = new File(beepUrl.getPath());
		String sound = soundFile.getAbsoluteFile().toString();
		
		String array = ArrayUtils.toString(fStep);
		array = array.replace("{", "[").replace("}","]");
		
		ProcessBuilder proBuilder = new ProcessBuilder("cmd","/c", gen, filename, sound, array);
		
		try 
		{
			Process pro = proBuilder.start();
			
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}


	public double[] loadBeep() throws URISyntaxException, UnsupportedAudioFileException, IOException
	{
		
		URL resUrl = this.getClass().getResource("beep.wav");
		File soundFile = new File(resUrl.toURI());
		
		
		
		AudioFileFormat format = AudioSystem.getAudioFileFormat(resUrl);
		AudioInputStream audioIn = new AudioInputStream(new FileInputStream(soundFile), format.getFormat(), format.getFrameLength());
		
		byte[] buffer = new byte[4096];
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		
		int length;
		while((length = audioIn.read(buffer)) != -1)
		{
			byteOut.write(buffer);
		}
		
		audioIn.close();
		byteOut.flush();
		byte[] dataByte = byteOut.toByteArray();


		// conversion to normalized float
		double[] audioFloats = new double[dataByte.length];
		for (int i = 0; i < dataByte.length; i++) 
		{
			audioFloats[i] = ((float)dataByte[i])/0x80;
		}
		
		return audioFloats;
	}


	
	
	
	
	
	
	

}
